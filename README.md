# ozex, the Z88 operating system exercizer

`Design and programming by Thierry PEYCRU, 2022`

This code performs various tests using the OZ standard API. It aims at measuring OZ efficiency against initial release (v2.2/3.0) and look for regressions during OZ developement.

##Methods :

Ozex is proposed as a BBC Basic program for running under pre-OZ 5.0 or an ELF executable for OZ 5.0 and later.

Methodology is to provide a test covering a specific area of the API. test duration should be between 30 and 120 seconds in order to obtain a significant result.

##Results :

Test results are given in milliseconds.

| Number | Test                            | OZ 3.0 | OZ 5.0 | Ratio  | rev.E21FF134 | Ratio  |
|--------|---------------------------------|--------|--------|--------|--------------|--------|
|1       | Interrupt overhead              |  34370 |  33840 | x 1.02 |  33840       | x 1.02 |
|2       | Memory allocation (chunk)       |  70840 |  32590 | x 2.17 |  32560       | x 2.18 |
|3       | Memory allocation (page)        |  24380 |  15130 | x 1.61 |  15100       | x 1.61 |
|4       | File opening/closure            |  77100 |  32080 | x 2.40 |  31760       | x 2.43 |
|5       | Memory to file copying (OS_Mv)  |  44930 |  19600 | x 2.29 |  19420       | x 2.31 |
|6       | Screen output (OS_Out)          |  31230 |  22370 | x 1.39 |  22370       | x 1.39 |
|7       | Screen output (GN_Sop)          |  36500 |  16280 | x 2.24 |  16250       | x 2.24 |
|8       | Pipedream map (OS_Map)          |  75070 |  17130 | x 4.38 |  16880       | x 4.45 |

##Comments :
When running the tests, it is impressive to see the Z88 with OZ 5.0 having finished minutes ago. I never imagined that differences would be so massive.

First test indirectly report interrupt overhead. Rewritten interrupts shows a global improvment of 2% available for the user.

Memory API (tests 2 and 3) have been heavily optimized, OS push frame removal, chunk pre-allocation, optimized MAT read/write...

It impacts the file i/o (test 5) too because nonetheless the file system has be fully optimized but it is also based on a faster memory allocation. Sector allocation is speed up by a ratio of more than twice. 

File opening (test 4) has been rewritten with an `OS_Dor / DR_OP` reason code used in `GN_Opf`.

Screen output (tests 6 and 7) has been rewritten in large parts without OS push frame. It reduces significanly overhead. String output has its own dedicated API in OZ5.0 and GN_Sop is just a wrapper now. Thus, avoiding continous bank switching and OS frame increase string output speed by more than twice. Same comments for `OS_Map` completly rebased.

##Todo :
`DC_Icl` with various CLI, especially the MTH, application switching...
