#!/bin/sh

# OZEX, OZ exercizer compilation script
# for elf binary and BBC Basic
# (C) Thierry Peycru, 2022

rm -f *.bin *.elf *.map *.err
mpm -bG -I../../oz/def ozex.asm
if test $? -eq 0; then
    # program compiled successfully, apply leading Z80 ELF header
    mpm -bG -nMap -I../../oz/def -oozex ozex-elf.asm
    # apply leading BBC Basic wrapper
    mpm -bG -nMap -I../../oz/def -oozex.bbc ozex-bbc.asm
fi
