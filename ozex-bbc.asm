; **************************************************************************************************
; OZEX, OZ exercizer FOR BBC Basic
;
; This file is part of the Z88 operating system, OZ      0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; ***************************************************************************************************

module OzexBbc

include "bastoken.def"
include "stdio.def"
include "ozex.inc"

;
;       BBC Basic wrapper
;
defc    BAS_ORG = $2300

org     BAS_ORG                                   ; default basic program area

.line_org
        defb    line_end - line_org             ; line length
        defw    1                               ; line number (zero in orginal version, verbose LIST)
        defm    BAS_IF, BAS_PAGE_G, "=&2300 ", BAS_LOMEM_P, "=16383:"
        defm    BAS_CALL, "&2320 ", BAS_ELSE, BAS_PRINT, '"', "PAGE!", '"'
        defb    CR                              ; end of line
.line_end
        defb    0                               ; program terminator
.bas_stk
        defw    0
        defb    $FF,$FF,$FF,$FF                 ; padding

.basic_entry                                    ; at $2320
        ld      hl, 0
        add     hl, sp
        ld      (bas_stk), hl                  ; save basic stack
        ld      hl, ($1FFE)                     ; use system stack
        ld      sp, hl
        call    EXEC_ORG
        ld      hl, (bas_stk)
        ld      sp, hl                          ; restore basic stack
        ret

; padding until elf org

        defs (EXEC_ORG - BAS_ORG - $PC) ($FF)
        binary "ozex.bin"
