; **************************************************************************************************
; OZEX, OZ exercizer main code
;
; This file is part of the Z88 operating system, OZ      0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; ***************************************************************************************************

module ozex

include "stdio.def"
include "memory.def"
include "handle.def"
include "error.def"
include "time.def"
include "integer.def"
include "fileio.def"
include "map.def"

include "ozex.inc"

org EXEC_ORG


.entry
        ld      hl, test_list
        ld      iy, test_results

.main_loop
        ld      e, (hl)
        inc     hl
        ld      d, (hl)
        inc     hl
        ld      a, e
        or      d
        jr      z, tests_done

        push    hl
        ex      de, hl
        ld      a, FF
        oz      OS_Out
        ld      a, (hl)
        ld      (iy+0), a                       ; byte 0 : test number
        add     a, '0'
        oz      OS_Out                          ; display test number
        inc     hl
        ld      de, 2                           ; result in ABC
        oz      GN_Gmt
        push    bc
        push    af
        ld      de, ret_here
        push    de                              ; push ret
        push    hl                              ; push test
        ret                                     ; and exec
.ret_here
        jr      c, test_ret_0
        xor     a                               ; test passed
.test_ret_0
        ld      (iy+1), a                       ; byte 1 : test status, passed (0) or error
        ld      de, 2                           ; result in ABC
        oz      GN_Gmt
        ld      l, c
        ld      h, b
        pop     bc
        pop     de
        or      a
        sbc     hl, de
        sbc     a, b
        ld      (iy+2), l
        ld      (iy+3), h
        ld      (iy+4), a
        ld      (iy+5), 0
        ld      bc, 6
        add     iy, bc
        pop     hl
        jr      main_loop

.exit
        oz      GN_Nln
        ld      hl, 0
        or      a
        ret

.tests_done
        ld      (iy+0), 0                       ; terminator
        ld      a, FF
        oz      OS_Out                          ; CLS
        ld      hl, test_results
.result_loop
        ld      a, (hl)
        or      a
        jr      z, exit
        ld      a, (ntest)
        and     3
        cp      3
        jr      nz, no_nln
        oz      GN_Nln
.no_nln
        ld      a, (ntest)
        add     a, '0'
        oz      OS_Out
        ld      a, ' '
        oz      OS_Out
        inc     hl
        ld      a, (hl)
        or      a
        jr      nz, report_error
        inc     hl
        ld      ix, phnd_Ohn
        ld      de, 0
        ld      a, 0
        oz      GN_Pdn
        ex      de, hl
        ld      hl, msg_msec
        oz      GN_Sop
        ex      de, hl
        ld      bc, 4
        add     hl, bc
        ld      a, (ntest)
        inc     a
        ld      (ntest), a
        jr      result_loop
.report_error
        oz      GN_Esp
        oz      GN_Soe
        ret

.msg_msec
        defm    "0 ms, ",0

.ntest
        defs    1 (1)

;       ----

;       vectors to test
;
;       each test should have an average duration of at least 10 seconds
;       granularity of 1/100s, significativity level of 5%, so 5s*2 = 10s
;       so around 20-30s duration should give quite good results
;
.test_list
        defw    test_compute_di
        defw    test_compute_ei
        defw    test_mal_chunk
        defw    test_mal_page
        defw    test_gnopf
        defw    test_osmv
        defw    test_osout
        defw    test_gnsop
        defw    test_osmap
        defw    0

;       ----

;       test intended to show indirect time taken by RST38 interrupt
;       just a big loop
;       concern : RST38
;       result  : less is better
;
.test_compute_di
        defb    1                               ; test #1
        di
        ld      b, 64
        ld      hl, 0
.tcp_0
        inc     hl                              ; 6
        ld      a, h                            ; 4
        or      l                               ; 4
        jr      nz, tcp_0                       ; 12/7
        djnz    tcp_0                           ; 13/8
        ei
        ret

.test_compute_ei
        defb    1                               ; test #1
        ld      b, 64
        ld      hl, 0
.tcp_1
        inc     hl                              ; 6
        ld      a, h                            ; 4
        or      l                               ; 4
        jr      nz, tcp_1                       ; 12/7
        djnz    tcp_1                           ; 13/8
        ret

; ( 645536*128*26 + 128*13 ) * 1/3276800
;   218102808     + 1664     * 0,000000305175781
;   66,56 sec
;
; OZ 5.0 : 67680ms -> 1120ms (1,65%) of time used by RST38
;          (same OZ on OZvm reports 73010ms)

;       ----

;       test memory chunk allocation
;       allocate chunks of 5 bytes
;       concern : OS_Mop / OS_Mal / OS_Mfr
;       result  : less is better
;
; OZ 5.0 : 98420ms (same OZ on OZvm reports 107910ms)
;
.test_mal_chunk
        defb    2                               ; test #2
        ld      b, 32
.tmc_l
        push    bc
        ld      a, MM_S3 | MM_MUL | MM_FIX
        ld      bc, 0
        oz      OS_Mop
        ret     c
        ld      de, 2048
.tmc_0
        xor     a
        ld      bc, 5
        oz      OS_Mal
        jr      c, tmc_x
        dec     de
        ld      a, d
        or      e
        jr      nz, tmc_0
.tmc_x
        push    af
        oz      OS_Mcl
        pop     af
        pop     bc
        ret     c
        djnz    tmc_l
        ret

;       ----

;       test memory page allocation
;       allocate full page (256 bytes)
;       concern : OS_Mop / OS_Mal / OS_Mfr
;       result  : less is better
;
; OZ 5.0 : 
;
.test_mal_page
        defb    3
        ld      b, 128
.tmp_l
        push    bc
        ld      a, MM_S3 | MM_MUL | MM_FIX
        ld      bc, 0
        oz      OS_Mop
        ret     c
        ld      de, 64
.tmp_0
        xor     a
        ld      bc, 256                         ; page
        oz      OS_Mal
        jr      c, tmp_x
        dec     de
        ld      a, d
        or      e
        jr      nz, tmp_0
.tmp_x
        push    af
        oz      OS_Mcl
        pop     af
        pop     bc
        ret     c
        djnz    tmp_l
        ret

;       ----

;       test file opening for output / closure / deletion
;       concern : GN_Opf, GN_Cl, GN_Del
;       result  : less is better
;
.test_gnopf
        defb    4
        ld      hl, 512
        ld      (testcnt), hl
.tgo_0
        ld      bc, 32
        ld      hl, fnametmp
        ld      de, buffer
        ld      a, OP_OUT
        oz      GN_Opf
        ret     c
        oz      GN_Cl
        ld      b, 0
        ld      hl, fnametmp
        oz      GN_Del
        ld      hl, (testcnt)
        dec     hl
        ld      a, h
        or      l
        ret     z
        ld      (testcnt), hl
        jr      tgo_0

;       ----

;       test file filling (sector allocation)
;       concern : GN_Opf, OS_Mv, GN_Cl, GN_Del
;       result  : less is better
;
.test_osmv
        defb    5
        ld      hl, 256
        ld      (testcnt), hl
.tmv_0
        ld      bc, 32
        ld      hl, fnametmp
        ld      de, buffer
        ld      a, OP_OUT
        oz      GN_Opf
        ret     c
        ld      hl, entry
        ld      de, 0
        ld      bc, 512
        oz      OS_Mv
        jr      c, $-1                          ; !! fixme later
        oz      GN_Cl
        ld      b, 0
        ld      hl, fnametmp
        oz      GN_Del
        ld      hl, (testcnt)
        dec     hl
        ld      a, h
        or      l
        ret     z
        ld      (testcnt), hl
        jr      tmv_0

;       ----

;       test screen output
;       concern : OS_Out
;       result  : less is better
;
.test_osout
        defb    6
        ld      hl, $8000
.too_0
        ld      a, '8'
        oz      OS_Out
        dec     hl
        ld      a, h
        or      l
        ret     z
        jr      too_0

;       ----

;       test string output
;       concern : GN_Sop
;       result  : less is better
;
.test_gnsop
        defb    7
        ld      de, 2048
.tgs_0
        ld      hl, msg_cc
        oz      GN_Sop
        dec     de
        ld      a, d
        or      e
        ret     z
        jr      tgs_0
.msg_cc
        defm    "CambridgeComputers",0

;       ----

;       test pipedream map
;       concern : OS_Map
;       result  : less is better
;
.test_osmap
        defb    8
        ld      bc, MP_Gra
        ld      hl, 255
        ld      a, '5'
        oz      OS_Map
        ret     c

        ld      hl, 256
.tom_0
        ld      (testcnt), hl
        ld      hl, mapline
        ld      bc, MP_Wr
        ld      de, 0
.tom_1
        ld      a, '5'
        oz      OS_Map
        ret     c
        inc     de
        ld      a, e
        and     @00111111
        ld      e, a
        or      d
        jr      nz, tom_1                
        ld      hl, (testcnt)
        dec     hl
        ld      a, h
        or      l
        jr      nz, tom_0
        ret

.mapline
        defs    256 (@10101010)

;       ----

.testcnt
        defs    2 (0)
.buffer
        defs    32 (0)
.fnametmp
        defm    ":RAM.-/test.tmp"

;------------------------------------------------------------------------------
; Workspace is located from here onwards (after end of program),
; allocated by Elf Loader
.Workspace
.test_results
